package com.marksandspencer;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestHarness {
    public static final String mainPage = "http://www.marksandspencer.com/";
    public static final long DEFAULT_TIMEOUT_IN_SECS = 120;

    private static final WebDriver webDriver = new ChromeDriver();
    private static final TestHarness INSTANCE = new TestHarness();

    public WebDriver getWebDriver() {
        webDriver.manage().timeouts().pageLoadTimeout(DEFAULT_TIMEOUT_IN_SECS, TimeUnit.SECONDS);
        return webDriver;
    }

    public static TestHarness getInstance() {
        return INSTANCE;
    }
}
