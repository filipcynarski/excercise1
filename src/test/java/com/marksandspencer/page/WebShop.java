package com.marksandspencer.page;

import org.fluentlenium.core.FluentPage;

import com.marksandspencer.TestHarness;

/**
 * @author : Filip Cynarski
 */
public class WebShop extends FluentPage {
    @Override
    public String getUrl() {
        return TestHarness.mainPage;
    }
}
