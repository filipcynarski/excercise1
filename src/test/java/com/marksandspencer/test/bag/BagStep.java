package com.marksandspencer.test.bag;

import static org.fluentlenium.core.filter.FilterConstructor.with;
import static org.fluentlenium.core.filter.FilterConstructor.withName;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.fluentlenium.core.FluentAdapter;
import org.fluentlenium.core.annotation.Page;
import org.fluentlenium.core.domain.FluentList;
import org.fluentlenium.core.domain.FluentWebElement;

import com.marksandspencer.TestHarness;
import com.marksandspencer.page.WebShop;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author : Filip Cynarski
 */
public class BagStep extends FluentAdapter {
    private static long MAX_WAIT_TIME = 30;

    @Page
    WebShop page;

    @Before
    public void before() {
        this.initFluent(TestHarness.getInstance().getWebDriver());
        this.initTest();
    }

    @Given(value = "I have added a (.*) to my bag")
    public void addToBag(String what) {
        visitPage();
        searchItemInShop(what);
        selectFirstItemOnSearchResult(what);
        selectFirstSizeOnTheList();
        addItemToTheBag();
    }

    private void visitPage() {
        goTo(page);
        await().atMost(TestHarness.DEFAULT_TIMEOUT_IN_SECS).untilPage().isLoaded();
    }

    @When(value = "I view the contents of my bag")
    public void viewBagContent() {
        FluentWebElement bagView = find(".basket.not-empty").first();
        click(bagView);
        await().atMost(MAX_WAIT_TIME).until("#basketForm").isPresent();
    }

    @Then(value = "I can see the contents of the bag include a (.*)")
    public void contentOfBagIncludeItem(String item) {
        FluentList bagViewItems = find(".product-item");
        assertTrue(bagViewItems.find("a", with("href").contains(item)).size() > 0);
    }

    @After
    public void after() {
        this.quit();
    }

    private void addItemToTheBag() {
        FluentWebElement addToBasket = find("input", with("data-adding").equalTo("Adding"), with("type").equalTo
                ("submit")).first();

        click(addToBasket);

        await().atMost(MAX_WAIT_TIME, TimeUnit.SECONDS).until(".added-to-bag").isPresent();
    }

    private void selectFirstSizeOnTheList() {
        await().atMost(MAX_WAIT_TIME, TimeUnit.SECONDS).until(".size-indicator").isPresent();
        FluentWebElement firstSize = find(".sizes.displayCell").find("td").first();
        click(firstSize);
    }

    private void selectFirstItemOnSearchResult(String what) {
        FluentWebElement firstProductLead = find(".prodAnchor", with("href").contains(what)).first();
        click(firstProductLead);

        await().atMost(MAX_WAIT_TIME, TimeUnit.SECONDS).until(".product-selection").isPresent();
    }

    private void searchItemInShop(String what) {
        FluentWebElement input = find("input", withName("searchTerm")).first();
        fill(input).with(what);
        click("#goButton");
        await().atMost(MAX_WAIT_TIME, TimeUnit.SECONDS).until(".product-listing").isPresent();
    }
}
