package com.marksandspencer.test.bag;

import org.junit.runner.RunWith;

import cucumber.api.junit.Cucumber;

/**
 * * @author : Filip Cynarski
 */
@RunWith(Cucumber.class)
@Cucumber.Options(features = "classpath:bag")
public class BagTest {

}
